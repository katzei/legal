#!/bin/bash
rst2pdf --config=./config.ini ./reglement/reglement.rst -o ./reglement/reglement.pdf
rst2pdf --config=./config.ini ./statuts/statuts.rst -o ./statuts/statuts.pdf
rst2pdf --config=./config.ini ./assemblees_generales/AGE_2024_07_26.rst -o ./assemblees_generales/AGE_2024_07_26.pdf
