.. footer::

	###Page###/###Total###

Statuts
=======

Article 1 - Dénomination
------------------------

Il est fondé entre les adhérents aux présents statuts une association régie par la loi du 1er juillet 1901 et le décret du 16 août 1901, ayant pour titre "Katzei".

Article 2 - Objet
-----------------

Cette association a pour buts :

* la fourniture de services libres à destination du grand public à travers le site de l'association,
* la promotion de l'auto-hébergement,
* la promotion du logiciel libre,
* le financement du logiciel libre.


Article 3 - Siège social
------------------------

`Le siège social est fixé au 19 rue Paul Banéat 35700 Rennes.`

Article 4 - Durée
-----------------

La durée de l’association est illimitée.

Article 5 - Composition
-----------------------

L'association se compose de :

* membres actifs,
* membres hébergeurs,
* membres bienfaiteurs.

Article 6 - Ressources
----------------------

Les ressources de l'association sont composées de :

* cotisations,
* dons ou legs,
* ventes de biens appartenant à l'association,
* ventes de services fournis par l'association,
* prestations de services.

Ces ressources peuvent être utilisées à toutes fins utiles comprenant notamment mais pas exclusivement l'achat ou la location de biens matériels ou immatériels, le financement de projets libres, la publicité ou le transport pour des évènements.

Article 7 - Admission
---------------------

Pour faire partie de l'association, il faut jouir de ses droits civiques. Il faut également être coopté par le Bureau qui statue, lors de chacune de ses réunions, sur les demandes d'admission présentées. Pour les candidats âgés de moins de 18 ans, il sera demandé la caution d’un représentant légal.

Article 8 - Membres et cotisations
----------------------------------

Sont membres actifs les personnes physiques qui ont versé une cotisation annuelle à prix libre avec un minimum défini au règlement intérieur.

Sont membres hébergeurs les personnes physiques hébergeant dans leurs locaux une ou plusieurs machines, physiques ou virtuelles, à destination des activités de l'association et qui auront été acceptés pour ce rôle selon les modalités définies au réglement intérieur. Les membres hébergeurs sont exemptés de cotisation.

Sont membres les personnes morales ayant fait un don dans l'année.

Seuls les membres actifs et les membres hébergeurs sont pourvus d'un droit de vote aux assemblées générales ordinaires et extraordinaires.

Article 9 - Radiations
----------------------

La qualité de membre se perd par :

* la démission communiquée au Bureau,
* le décès,
* la radiation prononcée par le Bureau pour motif grave,
* l'expiration de la cotisation.

Article 10 - Affiliation
------------------------

La présente association peut adhérer à d’autres associations, unions ou regroupements par décision du Bureau.

Article 11 - Bureau
-------------------

L'association est dirigée par un Bureau, constitué au minimum de 2 membres issus des membres actifs ou membres hébergeurs. Les membres du Bureau sont élus lors de l'assemblée générale ordinaire et pour une durée de 1 an, par les membres actifs et membres hébergeurs de l'association. Ils sont rééligibles plusieurs fois.

A chaque changement de sa composition et au moins une fois par an, le Bureau élit parmi ses membres :

* un Président,
* un Secrétaire,
* un Trésorier,

auxquels pourront s’adjoindre 1 ou 2 autres membres.

Les rôles de Président et de Trésorier doivent être distincts.


Article 12 - Assemblée générale ordinaire
-----------------------------------------

L'assemblée générale ordinaire comprend les membres actifs et les membres hébergeurs de l'association.
Elle se réunit au moins une fois par année calendaire, à une date fixée par le Bureau. Compte tenu de la nature immatérielle des activités de l'association, les assemblées générales peuvent se tenir à distance si besoin.

Au moins treize jours calendaires avant la date fixée, les membres actifs et membres hébergeurs sont convoqués par email.

L'ordre du jour doit apparaître sur la convocation.

Les membres actifs ou membres hébergeurs de l'association ne pouvant pas être présents peuvent donner procuration à un autre membre actif ou un autre membre hébergeur de l'association pour le représenter selon les modalités précisées dans le règlement intérieur.

L'assemblée délibère sur tous les points inscrits à l'ordre du jour. Les décisions sont prises selon les modalités précisées dans le règlement intérieur.


Article 13 - Assemblée générale extraordinaire
----------------------------------------------

L'assemblée générale extraordinaire comprend les membres actifs et les membres hébergeurs de l'association.

Elle peut être réunie par simple demande du Président, de la majorité des membres du Bureau ou de la majorité des membres actifs et des membres hébergeurs.

La convocation, la délibération et le vote se font suivant les mêmes modalités que pour l'assemblée générale ordinaire.

Seule l'assemblée générale extraordinaire est compétente pour modifier les statuts, décider la dissolution, la fusion de l'association avec toute autre association poursuivant le même objet ou les mêmes orientations.

Article 14 - Règlement intérieur
--------------------------------

Le fonctionnement de l'association est régi par son règlement intérieur adopté en assemblée générale.

Les modifications ultérieures du règlement intérieur se font suivant les modalités précisées dans la version en vigueur de ce même règlement.

Article 15 - Dissolution
------------------------

En cas de dissolution volontaire ou forcée, l'assemblée générale extraordinaire statue de la dévolution du patrimoine de l'association.

Elle désigne les organismes à but non lucratif poursuivant des objectifs analogues qui recevront le reliquat de l'actif après paiement de toutes les dettes et charges de l'association et de liquidation. Elle nomme, pour assurer les opérations de liquidation, un ou plusieurs liquidateurs parmi ses membres actifs ou membres hébergeurs qui seront investis de tous les pouvoirs nécessaires.

----

Les présents statuts ont été approuvés par l'assemblée générale constitutive du 26 mai 2020.
Les présents statuts ont été révisés par :

* l'assemblée générale extraordinaire du 07 juillet 2023.
* l'assemblée générale extraordinaire du 26 juillet 2024.

.. class:: signature

Président : Raphaël Paoloni

.. image:: 2020_05_26_Signature_PAOLONI_Raphael.png
  :width: 400

.. class:: signature

Secrétaire : Jean-Marc Lefrançois

.. image:: 2020_05_24_Signature_LEFRANCOIS_JeanMarc.png
  :width: 400