.. sectnum::

.. footer::

	###Page###/###Total###

Règlement intérieur
===================


Objet
-----

Le présent règlement intérieur complète et précise les statuts de l'association "Katzei", approuvés par l'assemblée constitutive du 18 avril 2020.

Membres
-------

Membres actifs
^^^^^^^^^^^^^^

Adhésion
~~~~~~~~

Pour adhérer à l'association en tant que membre actif, un aspirant doit être une personne physique et faire parvenir sa candidature au Bureau par un moyen à sa convenance.

Cette candidature est alors étudiée par le Bureau et, si elle est acceptée, l'aspirant devra verser annuellement une cotisation à l'association pour bénéficier de son statut de membre actif.

Dans tous les cas, le statut de membre actif n'est accordé que si l'aspirant :

* adhère aux buts de l'association,
* accepte les statuts et le règlement intérieur de l'association,
* voit sa candidature acceptée par le Bureau,
* est à jour de cotisation.

Cotisation
~~~~~~~~~~

Le prix de la cotisation est libre avec un minimum dépendant de la catégorie.

* **Cotisation normale**: Cette cotisation s'applique à toute personne non éligible pour la cotisation réduite. La cotisation minimale est de 20 EUR par an et reste libre au-delà.

* **Cotisation réduite**: Cette cotisation s'applique aux personnes étudiantes, sans emploi ou retraitées. Pour bénéficier de cette réduction, la personne doit faire parvenir, en plus de sa cotisation, une attestation sur l'honneur qu'elle se trouve dans l'une des situations précitées. La cotisation minimale est alors de 10 EUR par an et reste libre au-delà.

Droits
~~~~~~

Un membre actif dispose d'un droit de vote aux assemblées générales (ordinaires et extraordinaires) et peut être élu au Bureau.

Membres hébergeurs
^^^^^^^^^^^^^^^^^^

Adhésion
~~~~~~~~

Pour adhérer à l'association en tant que membre hébergeur, un aspirant doit être une personne physique et faire parvenir sa candidature au Bureau par un moyen à sa convenance.

Pour pouvoir postuler en tant que membre hébergeur, l'aspirant doit proposer d'héberger une ou plusieurs machines (physiques ou virtuelles) pour le bénéfice de l'association. Cet hébergement doit se faire selon les règles spécifiées plus loin dans ce document.

L'adhésion ou la promotion au rang de membre hébergeur sera étudiée à la prochaine assemblée générale, en présence de l'aspirant.

Révocation
~~~~~~~~~~

Si un membre hébergeur ne répond pas à ses obligations d'hébergement de services, celui-ci peut voir ce statut révoqué par le Bureau.

Le Bureau décide si cette révocation implique que la personne destituée de son statut de membre hébergeur est radiée de l'association ou considérée dès lors comme membre actif jusqu'à la fin de l'année en cours.

Cotisation
~~~~~~~~~~

Le prix de la cotisation est libre sans minimum.

Droits
~~~~~~

Un membre hébergeur dispose d'un droit de vote aux assemblées générales (ordinaires et extraordinaires) et peut être élu au Bureau.

Membres bienfaiteurs
^^^^^^^^^^^^^^^^^^^^

Adhésion
~~~~~~~~

Pour adhérer à l'association en tant que membre bienfaiteur, un aspirant doit être une personne morale et faire parvenir sa candidature au Bureau par un moyen à sa convenance.

Sa candidature est alors étudiée par le Bureau et, si elle est acceptée, il devra verser annuellement une cotisation à l'association pour bénéficier de son statut.

Cotisation
~~~~~~~~~~

Le prix de la cotisation est libre avec un minimum de 50 EUR par an.

Droits
~~~~~~

Un membre bienfaiteur ne dispose pas de droit de vote.

Durée d'adhésion et modalités de cotisation
-------------------------------------------

L'adhesion est valable de la date à laquelle elle est contractée jusqu'au prochain 31 décembre.

Le tarif de l'adhésion des membres actifs est divisé par deux si l'adhésion a lieu après le 30 juin de l'année en cours.

Procédure de radiation
----------------------

L'appellation motif grave comprend notamment le non-paiement d'une cotisation à sa date d'exigibilité et le non-respect des règles d'hébergement.

Lors de la procédure de radiation pour motif grave, le ou les membres responsables doivent pouvoir s'exprimer devant le Bureau avant que celui-ci décide ou non de prononcer la radiation.

Réunion du Bureau
-----------------

Le Bureau se réunit au moins une fois par année calendaire sur convocation du Pésident ou du Trésorier, aussi souvent que nécessaire sur la demande de la moitié de ses membres, de la moitié des membres actifs, ou en cas de besoin de statuer sur l'adhésion ou la révocation voire la radiation d'un membre.

La convocation doit être transmise par courrier électronique ou postal aux membres du Bureau au moins une semaine avant la date de réunion ainsi qu'à l'ensemble des membres. Sauf indications contraires mentionnées dans la convocation, les réunions du Bureau se déroulent à huis clos.

Tout membre du Bureau absent sans excuse à 3 réunions consécutives pourra être considéré comme démissionnaire.
Les membres du Bureau ne peuvent être représentés par procuration.

Le Bureau peut se réunir physiquement ou par n'importe quel moyen approuvé par la totalité de ses membres.

Le quorum est fixé à la majorité absolue des membres du Bureau.
Les réunions font l'objet d'un procès-verbal.

Procuration
-----------

Un membre indisponible à la date d'une assemblée générale peut se faire représenter par un autre membre auquel il aura confié une procuration.

La procuration doit clairement identifier le mandant et le mandataire.

Un membre ne peut être mandataire de plus de deux procurations.

Vote
----

Lors de l'élection du Bureau en assemblée générale, les candidats sont approuvés individuellement via un vote à la majorité simple des membres actifs présents ou représentés.

L'assemblée générale ne statue pas sur la répartition des rôles dans le Bureau.

Lors d'un vote au sein du Bureau, le vote se fait à la majorité simple des membres du Bureau. Si une égalité survient, la voix du Président est prépondérante.

Règles d'hébergement
--------------------

Matériel
^^^^^^^^

Le matériel mis à disposition de l'association par un membre reste la propriété de ce membre.


Localisation
^^^^^^^^^^^^

Les machines physiques gérant les services hébergés pour l'association doivent être possédés par un membre de l'association et situés dans des locaux contrôlés par au moins un membre de l'association.

Dans le cas où le service à héberger ne supporterait pas un tel hébergement, une dérogation pourra être acceptée par le Bureau. Une telle dérogation doit être communiquée aux membre de l'association et, s'il s'agit d'un service annoncé sur le site, indiquée sur la page du site.


Confidentialité
^^^^^^^^^^^^^^^

La confidentialité des données des utilisateurs est au centre de la démarche de l'association. Cette confidentialité implique de suivre certaines règles.

Accès aux données
~~~~~~~~~~~~~~~~~

Les administrateurs de services hébergés pour l'association s'interdisent d'accéder aux données des utilisateurs en dehors de besoins techniques, de demandes de l'utilisateur ou des cas prévus par la loi.

Communication à un tiers
~~~~~~~~~~~~~~~~~~~~~~~~

Les administrateurs des services hébergés par l'association s'interdisent de transmettre des données des utilisateurs à un tiers sauf à la demande de l'utilisateur ou dans les cas prévus par la loi.

Services statistiques
~~~~~~~~~~~~~~~~~~~~~

L'utilisation de services statistiques pour connaitre la fréquentation des services est autorisée. Les données de navigation étant considérées comme des données des utilisateurs, il est interdit d'avoir recours à un service externe à l'association.

Interopérabilité
^^^^^^^^^^^^^^^^

Dans le cas où un utilisateur souhaiterait migrer d'un service de l'association vers un autre service, le membre hébergeur de ce service doit fournir toutes les données nécessaires pour faciliter la migration de l'utilisateur.

Révision du règlement intérieur
-------------------------------

Tout projet visant à modifier le règlement intérieur de l'association doit être soumis à l'approbation de l'assemblée générale ordinaire ou extraordinaire.


Le présent règlement intérieur à été approuvé par l'assemblée générale constitutive du 26 mai 2020.

.. class:: signature

Président : Raphaël Paoloni

.. image:: 2020_05_26_Signature_PAOLONI_Raphael.png
  :width: 400

.. class:: signature

Trésorier : Jean-Marc Lefrançois

.. image:: 2020_05_24_Signature_LEFRANCOIS_JeanMarc.png
  :width: 400
