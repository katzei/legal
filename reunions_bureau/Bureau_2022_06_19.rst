.. sectnum::
	
	.. footer::
	
		###Page###/###Total###
	
	Réunion de Bureau
	=================
	Déroulement
	-----------
	
	Membres présents
	^^^^^^^^^^^^^^^^
	* Fratibus
	* Meewan
	* Zalahiston (secrétaire de séance)
	
	Modalités
	^^^^^^^^^
	* Lieu : Dématérialisé (audioconférence)
	* Jour : le 19 juin 2022
	* Début : 21h25
	* Fin : 21h40
		
	Nomination des membres
	----------------------
	* Président·e : Meewan
	* Trésorier·ère : Fratibus
	* Secrétaire : Zalahiston
	