.. sectnum::
	
	.. footer::
	
		###Page###/###Total###
	
Réunion de Bureau
=================
Déroulement
-----------

Membres présents
^^^^^^^^^^^^^^^^
* Fratibus
* Meewan
* Zalahiston (secrétaire de séance)

Modalités
^^^^^^^^^
* Lieu : Autheuil-Authouillet
* Jour : le 26 juillet 2024
* Début : 19h09
* Fin : 19h12
	
Nomination des membres
----------------------
* Président·e : Meewan
* Trésorier·ère : Fratibus
* Secrétaire : Zalahiston